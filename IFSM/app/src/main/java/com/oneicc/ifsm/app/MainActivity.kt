package com.oneicc.ifsm.app

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.location.*
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.activity.ActivityModel
import com.oneicc.ifsm.common.activity.ActivityRequestModel
import com.oneicc.ifsm.common.base.BaseActivity
import com.oneicc.ifsm.common.model.Customer
import com.oneicc.ifsm.common.network.Api
import com.oneicc.ifsm.common.network.NetworkClient
import com.oneicc.ifsm.common.util.BusAction
import com.oneicc.ifsm.common.util.Constants
import com.oneicc.ifsm.common.util.DateTimeUtils
import com.oneicc.ifsm.common.util.Utils
import com.oneicc.ifsm.feature.user.LoginFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import timber.log.Timber

class MainActivity : BaseActivity(R.layout.activity_main) {
    private val appPermissions: List<String> = listOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var activityApi: Api

    var location: Location? = null

    companion object {
        const val PERMISSION_REQUEST_CODE = 304
        var currentDocId: String? = null
        var currentAction = Constants.ACTION_NON
        var currentCustomer: Customer? = null
        var currentActivityId: Int? = null
        var currentVisitActivityId: Int? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        activityApi = NetworkClient.getRetrofitForTracking(this).create(Api::class.java)
        if (checkPermission()) {
            setUpLocationProvider()
        }
        setSupportActionBar(toolbar)
        Utils.exportDatabase(this)
        setFragment(LoginFragment())
    }

    private fun checkPermission(): Boolean {
        val neededPermission =
            appPermissions.filter { ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED }

        if (neededPermission.isNotEmpty()) {
            ActivityCompat.requestPermissions(this, neededPermission.toTypedArray(), PERMISSION_REQUEST_CODE)
            return false
        }

        return true
    }

    @SuppressLint("MissingPermission")
    private fun setUpLocationProvider() {
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            this.location = location
            Timber.i("Callback lastLocation $location")
        }
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult?) {
            if (result == null) return
            result.locations.forEach {
                if (it != null) {
                    location = it
                    Timber.i("Update location $it")
                }
            }
            location?.let {
                EventBus.getDefault().post(BusAction.Location(it))
                onReceiveLocation(it)
            }
            stopLocationUpdate()
        }
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdate() {
        val looper = Looper.myLooper()
        val locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = 20 * 1000
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, looper)
    }

    fun stopLocationUpdate() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun doAction(action: BusAction.DoAction) {
        currentAction = action.action
        startLocationUpdate()
    }

    fun onReceiveLocation(location: Location) {
        when (currentAction) {
            Constants.ACTION_START_VISIT -> {
                addSalesActivity(
                    ActivityModel(
                        startDate = DateTimeUtils.convertToSQLFormatString(DateTimeUtils.getToday()),
                        customerCode = currentCustomer?.customerCode,
                        activityCode = Constants.ACTIVITY_CODE_VISIT,
                        startLongitude = location.longitude,
                        startLatitude = location.latitude
                    ), true
                )
            }
            Constants.ACTION_END_VISIT -> {
                updateSalesActivity(
                    ActivityModel(
                        id = currentVisitActivityId,
                        endDate = DateTimeUtils.convertToSQLFormatString(DateTimeUtils.getToday()),
                        customerCode = currentCustomer?.customerCode,
                        activityCode = Constants.ACTIVITY_CODE_VISIT,
                        endLongitude = location.longitude,
                        endLatitude = location.latitude
                    )
                )
                clearVisitData()
            }
            Constants.ACTION_START_ORDER -> {
                addSalesActivity(
                    ActivityModel(
                        startDate = DateTimeUtils.convertToSQLFormatString(DateTimeUtils.getToday()),
                        customerCode = currentCustomer?.customerCode,
                        activityCode = Constants.ACTIVITY_CODE_ORDER,
                        startLongitude = location.longitude,
                        startLatitude = location.latitude
                    ), false
                )
            }
            Constants.ACTION_END_ORDER -> {
                updateSalesActivity(
                    ActivityModel(
                        id = currentActivityId,
                        endDate = DateTimeUtils.convertToSQLFormatString(DateTimeUtils.getToday()),
                        customerCode = currentCustomer?.customerCode,
                        activityCode = Constants.ACTIVITY_CODE_ORDER,
                        endLongitude = location.longitude,
                        endLatitude = location.latitude,
                        documentId = currentDocId
                    )
                )
                clearActionData()
            }
        }
        currentAction = Constants.ACTION_NON
    }

    private fun addSalesActivity(salesActivity: ActivityModel, isVisitActivity: Boolean) {
        val json = Utils.gson().toJson(ActivityRequestModel(salesActivity))
        val body = RequestBody.create(MediaType.parse("application/json"), json)

        compositeDisposable.add(
            activityApi.addActivity(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (isVisitActivity) {
                        currentVisitActivityId = it.data
                    } else {
                        currentActivityId = it.data
                    }
                }, {
                    Timber.e("addSalesActivity $it")
                })
        )
    }

    private fun updateSalesActivity(salesActivity: ActivityModel) {
        val json = Utils.gson().toJson(ActivityRequestModel(salesActivity))
        val body = RequestBody.create(MediaType.parse("application/json"), json)

        compositeDisposable.add(
            activityApi.updateActivity(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.i("updateSalesActivity $it")
                }, {
                    Timber.e("updateSalesActivity $it")
                })
        )
    }

    private fun clearActionData() {
        currentAction = Constants.ACTION_NON
        currentActivityId = null
    }

    private fun clearVisitData() {
        currentDocId = null
        currentAction = Constants.ACTION_NON
        currentCustomer = null
        currentActivityId = null
        currentVisitActivityId = null
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    fun setUpTitleToolbar() {
        btnToolbarLeft.visibility = View.GONE
        btnToolbarRight.visibility = View.GONE
        tvToolbarTitle.visibility = View.VISIBLE
        tvToolbarTitle.text = getString(R.string.app_name)
    }

    fun setUpHomeToolbar() {
        btnToolbarLeft.visibility = View.VISIBLE
        btnToolbarLeft.setColorFilter(Color.WHITE)
        btnToolbarLeft.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_sync))
        btnToolbarLeft.setOnClickListener {
            Toast.makeText(this, "Dữ liệu đã được cập nhật!", Toast.LENGTH_SHORT).show()
        }

        tvToolbarTitle.visibility = View.GONE

        btnToolbarRight.visibility = View.VISIBLE
        btnToolbarRight.setColorFilter(Color.TRANSPARENT) //Remove tint to load image
        val size = Utils.dpToPx(24)

        Glide.with(this).load("https://images.gr-assets.com/users/1484400626p6/63853713.jpg")
            .centerInside().override(size, size)
            .apply(RequestOptions.circleCropTransform())
            .placeholder(R.drawable.default_avatar).into(btnToolbarRight)

        btnToolbarRight.setOnClickListener {
            Toast.makeText(this, "Xin chào, chúc bạn có một ngày làm việc hiệu quả!", Toast.LENGTH_SHORT).show()
        }
    }

    fun setUpTitleAndBackToolbar(title: String) {
        tvToolbarTitle.visibility = View.VISIBLE
        tvToolbarTitle.text = title

        btnToolbarLeft.visibility = View.VISIBLE
        btnToolbarLeft.setColorFilter(Color.WHITE)
        btnToolbarLeft.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_back))
        btnToolbarLeft.setOnClickListener {
            onBackPressed()
        }

        btnToolbarRight.visibility = View.GONE
    }

    fun setUpTitleAndBackAndRightToolbar(title: String, rightOnClick: () -> Unit = {}) {
        tvToolbarTitle.visibility = View.VISIBLE
        tvToolbarTitle.text = title

        btnToolbarLeft.visibility = View.VISIBLE
        btnToolbarLeft.setColorFilter(Color.WHITE)
        btnToolbarLeft.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_back))
        btnToolbarLeft.setOnClickListener {
            onBackPressed()
        }

        btnToolbarRight.visibility = View.VISIBLE
        btnToolbarRight.setColorFilter(Color.WHITE)
        btnToolbarRight.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_sync))
        btnToolbarRight.setOnClickListener {
            rightOnClick.invoke()
        }
    }
}
