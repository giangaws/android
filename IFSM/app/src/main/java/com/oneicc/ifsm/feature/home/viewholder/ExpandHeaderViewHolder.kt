package com.oneicc.ifsm.feature.home.viewholder

import android.view.View
import com.oneicc.ifsm.common.util.AnimUtils
import com.oneicc.ifsm.common.view.model.ExpandHeader
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import kotlinx.android.synthetic.main.item_expand_header.view.*

class ExpandHeaderViewHolder(view: View) : GroupViewHolder(view) {

    fun bind(data: ExpandHeader) {
        if (data.icon != -1) {
            itemView.imgHeaderIcon.setImageResource(data.icon)
        }
        itemView.tvHeaderTitle.text = data.title
        itemView.tvHeaderDetail.text = data.description
        if (data.children.isNullOrEmpty()) {
            itemView.imgGroupArrow.visibility = View.GONE
            itemView.vExpandHeader.setOnClickListener {
                data.onClick.invoke()
            }
        } else {
            itemView.imgGroupArrow.visibility = View.VISIBLE
            itemView.setOnClickListener(this)
        }
    }

    override fun expand() {
        super.expand()
        AnimUtils.expandAnimation(itemView.imgGroupArrow)
    }

    override fun collapse() {
        super.collapse()
        AnimUtils.collapseAnimation(itemView.imgGroupArrow)
    }
}