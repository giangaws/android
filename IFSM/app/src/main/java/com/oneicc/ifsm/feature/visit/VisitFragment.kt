package com.oneicc.ifsm.feature.visit

import android.widget.LinearLayout
import com.oneicc.ifsm.R
import com.oneicc.ifsm.app.MainActivity
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.view.model.FragmentPager
import com.oneicc.ifsm.feature.visit.adapter.TabAdapter
import kotlinx.android.synthetic.main.fragment_visit.*

class VisitFragment : BaseFragment(R.layout.fragment_visit) {

    override fun initData() {
        parentActivity?.setUpTitleAndBackToolbar(MainActivity.currentCustomer!!.customerCode)
    }

    override fun initView() {
        vpVisit.adapter = TabAdapter(childFragmentManager, ArrayList(
                listOf(
                        FragmentPager(VisitMainFragment(), getString(R.string.visit_tab_main)),
                        FragmentPager(VisitMapFragment(), getString(R.string.visit_tab_map)),
                        FragmentPager(VisitOrderHistoryFragment(), getString(R.string.visit_tab_order_history))
                )
        ))
        tlVisit.setupWithViewPager(vpVisit)
        lock()
    }

    /**
     * Lock tạm thời, demo xong mở ra sau
     */
    private fun lock() {
        val tabStrip = tlVisit.getChildAt(0) as LinearLayout
        for (i in 1 until tabStrip.childCount) {
            tabStrip.getChildAt(i).setOnTouchListener { _, _ -> true }
        }
    }

    override fun onDestroy() {
        MainActivity.currentCustomer = null
        super.onDestroy()
    }
}