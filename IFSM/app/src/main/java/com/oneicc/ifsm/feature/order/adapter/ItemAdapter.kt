package com.oneicc.ifsm.feature.order.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseAdapter
import com.oneicc.ifsm.common.base.BaseViewHolder
import com.oneicc.ifsm.feature.order.model.ProductUiModel
import kotlinx.android.synthetic.main.item_item.view.*
import java.text.NumberFormat

class ItemAdapter(data: ArrayList<ProductUiModel>) : BaseAdapter<ProductUiModel, ItemAdapter.ViewHolder>(data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_item, parent, false))
    }

    class ViewHolder(view: View) : BaseViewHolder<ProductUiModel>(view) {
        override fun bind(item: ProductUiModel, position: Int) {
            itemView.tvItemCode.text = item.item.itemCode
            itemView.tvItemName.text = item.item.itemName
            itemView.imgPromotion.visibility = if (item.item.isPromotion) View.VISIBLE else View.GONE
            itemView.tvQuantity.text = item.quantity.toString()
            itemView.tvPrice.text = NumberFormat.getInstance().format(item.stock[0].price) + " VNĐ"

            if (item.stock[0].inStock > 0) {
                itemView.btnPlus.visibility = View.VISIBLE
                itemView.btnMinus.visibility = View.VISIBLE
            } else {
                itemView.btnPlus.visibility = View.INVISIBLE
                itemView.btnMinus.visibility = View.INVISIBLE
            }

            itemView.btnMinus.setOnClickListener {
                item.quantity--
                if (item.quantity <= 0) {
                    item.quantity = 0
                }
                itemView.tvQuantity.text = item.quantity.toString()
            }
            itemView.btnPlus.setOnClickListener {
                item.quantity++
                if (item.quantity > item.stock[0].inStock) {
                    item.quantity = item.stock[0].inStock
                }
                itemView.tvQuantity.text = item.quantity.toString()
            }
        }
    }
}