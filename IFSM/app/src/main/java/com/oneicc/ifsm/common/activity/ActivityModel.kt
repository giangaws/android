package com.oneicc.ifsm.common.activity

import com.google.gson.annotations.Expose

data class ActivityModel(
    val icon: Int = -1,
    val name: String = "",
    val onClick: () -> Unit = {},
    val enable: Boolean = true,
    @Expose
    val id: Int? = null,
    @Expose
    val startDate: String? = null,
    @Expose
    val startLongitude: Double? = null,
    @Expose
    val startLatitude: Double? = null,
    @Expose
    val endDate: String? = null,
    @Expose
    val endLongitude: Double? = null,
    @Expose
    val endLatitude: Double? = null,
    @Expose
    val distributorId: Int = 1,
    @Expose
    val routeId: Int = 1,
    @Expose
    val activityCode: String? = null,
    @Expose
    val documentId: String? = null,
    @Expose
    val customerCode: String? = null
)