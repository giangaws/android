package com.oneicc.ifsm.feature.order

import android.animation.ObjectAnimator
import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import com.oneicc.ifsm.R
import com.oneicc.ifsm.app.MainActivity
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.model.Customer
import com.oneicc.ifsm.common.model.SOHeader
import com.oneicc.ifsm.common.model.SOLine
import com.oneicc.ifsm.common.network.Api
import com.oneicc.ifsm.common.network.NetworkClient
import com.oneicc.ifsm.common.util.*
import com.oneicc.ifsm.feature.order.adapter.OrderConfirmAdapter
import com.oneicc.ifsm.feature.visit.VisitFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_order_confirm.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import java.text.NumberFormat

class OrderConfirmFragment : BaseFragment(R.layout.fragment_order_confirm) {
    private lateinit var data: ArrayList<SOLine>
    private lateinit var header: SOHeader
    private val customer: Customer = MainActivity.currentCustomer!!
    private var discount = 0.0

    companion object {
        const val DATA_KEY = "DATA_KEY_ORDER_CONFIRM"

        fun getInstance(data: ArrayList<SOLine>): OrderConfirmFragment {
            val fragment = OrderConfirmFragment()
            val arguments = Bundle()
            arguments.putParcelableArrayList(DATA_KEY, data)
            fragment.arguments = arguments
            return fragment
        }
    }

    override fun initData() {
        parentActivity?.setUpTitleAndBackToolbar(getString(R.string.order_confirm_title))
        data = arguments?.getParcelableArrayList(DATA_KEY)!!

        val total = data.sumByDouble { it.lineTotal }

        header = SOHeader(
                null,
                customer.customerCode,
                DateTimeUtils.convertToSOPostFormatString(DateTimeUtils.getToday()),
                7,
                total,
                "",
                data
        )
    }

    override fun initView() {
        tvCustomerName.text = customer.customerName
        tvCustomerCode.text = customer.customerCode
        tvContact.text = customer.contact
        tvTotal.text = ""
        tvAfterTax.text = NumberFormat.getInstance().format(header.docTotal)
        edNote.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                header.comments = p0.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        btnPromotionCalculate.setOnClickListener {
            discount = header.docTotal * 0.1
            val total = header.docTotal - discount
            tvDiscountPercent.text = "10%"
            tvDiscount.text = "Chương trình khuyến mãi tháng 7/2019\nGiảm 10% trên tổng đơn hàng"
            tvTotal.text = NumberFormat.getInstance().format(total)
            it.isEnabled = false

            AnimUtils.blinkAnimation(tvDiscount)
            AnimUtils.blinkAnimation(tvDiscountPercent)
            AnimUtils.blinkAnimation(tvTotal)
        }

        btnOrderConfirm.setOnClickListener {
            btnOrderConfirm.text = getString(R.string.order_confirm_btn_confirm_loading)
            btnOrderConfirm.isEnabled = false
            confirmOrder()
        }

        btnAddItem.setOnClickListener {
            pop()
        }

        val adapter = OrderConfirmAdapter(data)
        rvOrderConfirm.addItemDecoration(DividerItemDecoration(rvOrderConfirm.context, DividerItemDecoration.VERTICAL))
        rvOrderConfirm.adapter = adapter
    }

    private fun confirmOrder() {
        val animator = ObjectAnimator.ofFloat(btnOrderConfirm, View.ALPHA, 0.5f, 1f)
        animator.duration = 500L
        animator.repeatCount = 10
        animator.start()

        val json = Utils.gson().toJson(header)
        val body = RequestBody.create(MediaType.parse("application/json"), json)

        val api = NetworkClient.getRetrofit(context!!).create(Api::class.java)
        compositeDisposable.add(
                api.postOrder(body).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (!it.errorMessage.isNullOrEmpty()) {
                                animator.end()
                                val builder = AlertDialog.Builder(context)
                                builder.setTitle(R.string.error)
                                builder.setMessage(it.errorMessage)
                                val dialog = builder.create()
                                dialog.show()
                                btnOrderConfirm.text = getString(R.string.order_confirm_btn_confirm)
                                btnOrderConfirm.isEnabled = true
                            } else {
                                animator.end()
                                MainActivity.currentDocId = it.refCode
                                EventBus.getDefault().post(BusAction.DoAction(Constants.ACTION_END_ORDER))
                                Toast.makeText(context, R.string.order_confirm_post_success, Toast.LENGTH_LONG).show()
                                popTo(VisitFragment::class.java)
                            }
                        }, {
                            animator.end()
                            Toast.makeText(context, it.message.toString(), Toast.LENGTH_SHORT).show()
                            Log.e("OrderConfirmFragment", it.toString())
                        })
        )
    }
}