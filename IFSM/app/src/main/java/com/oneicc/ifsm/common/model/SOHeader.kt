package com.oneicc.ifsm.common.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.oneicc.ifsm.common.base.BaseModel
import java.util.*

@Entity(indices = [Index("id")])
data class SOHeader(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,
    @Expose
    var cardCode: String = "", //Mã khách hàng
    @Expose
    var postingDate: String = "1980-01-01", //Ngày tạo đơn
    @Expose
    var slpCode: Int = 0, //Mã sale person
    @Expose
    var docTotal: Double = 0.0, //Giá trị đơn hàng
    @Expose
    var comments: String = "", //Ghi chú
    @Expose
    @Ignore
    var documentLines: List<SOLine> = ArrayList()
) : BaseModel()