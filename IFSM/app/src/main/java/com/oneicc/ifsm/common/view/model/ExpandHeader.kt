package com.oneicc.ifsm.common.view.model

import com.oneicc.ifsm.common.util.Constants
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

data class ExpandHeader(
    val headerTitle: String,
    val children: ArrayList<ExpandChild> = ArrayList(),
    val description: String = "",
    val icon: Int = Constants.DEFAULT_ICON,
    val onClick: () -> Unit = {}
) : ExpandableGroup<ExpandChild>(headerTitle, children)