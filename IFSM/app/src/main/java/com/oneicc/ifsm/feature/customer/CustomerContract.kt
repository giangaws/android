package com.oneicc.ifsm.feature.customer

import com.oneicc.ifsm.common.base.BaseDatabaseView
import com.oneicc.ifsm.common.model.Customer

interface CustomerContract {
    interface View : BaseDatabaseView<List<Customer>>

    interface Presenter {
        fun loadData()
    }
}