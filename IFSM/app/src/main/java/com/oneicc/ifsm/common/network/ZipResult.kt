package com.oneicc.ifsm.common.network

data class ZipResult<T : Any, T2 : Any>(val result1: T, val result2: T2)