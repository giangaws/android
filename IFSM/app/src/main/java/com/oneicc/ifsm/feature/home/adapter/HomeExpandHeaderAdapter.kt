package com.oneicc.ifsm.feature.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.model.CustomerExpandChild
import com.oneicc.ifsm.common.util.Constants
import com.oneicc.ifsm.common.view.model.ExpandHeader
import com.oneicc.ifsm.feature.home.model.HeaderOnlyExpandChild
import com.oneicc.ifsm.feature.home.model.HomeExpandChild
import com.oneicc.ifsm.feature.home.viewholder.CustomerChildViewHolder
import com.oneicc.ifsm.feature.home.viewholder.ExpandHeaderViewHolder
import com.oneicc.ifsm.feature.home.viewholder.HeaderOnlyChildViewHolder
import com.oneicc.ifsm.feature.home.viewholder.HomeChildViewHolder
import com.thoughtbot.expandablerecyclerview.MultiTypeExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder

class HomeExpandHeaderAdapter(private val fragment: BaseFragment, header: MutableList<ExpandHeader>) :
    MultiTypeExpandableRecyclerViewAdapter<ExpandHeaderViewHolder, ChildViewHolder>(header) {

    override fun onCreateGroupViewHolder(parent: ViewGroup, viewType: Int): ExpandHeaderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_expand_header, parent, false)
        return ExpandHeaderViewHolder(view)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup, viewType: Int): ChildViewHolder {
        return when (viewType) {
            Constants.CHILD_TYPE_HEADER_ONLY -> {
                val view =
                    LayoutInflater.from(parent.context).inflate(R.layout.item_expand_child_header_only, parent, false)
                HeaderOnlyChildViewHolder(view)
            }
            Constants.CHILD_TYPE_HOME -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_expand_child_home, parent, false)
                HomeChildViewHolder(view)
            }
            Constants.CHILD_TYPE_CUSTOMER -> {
                val view =
                    LayoutInflater.from(parent.context).inflate(R.layout.item_expand_child_customer, parent, false)
                CustomerChildViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid viewType")
        }
    }

    override fun onBindGroupViewHolder(holder: ExpandHeaderViewHolder?, flatPosition: Int, group: ExpandableGroup<*>?) {
        holder?.bind((group as ExpandHeader))
    }

    override fun onBindChildViewHolder(
        holder: ChildViewHolder?,
        flatPosition: Int,
        group: ExpandableGroup<*>?,
        childIndex: Int
    ) {
        when (getItemViewType(flatPosition)) {
            Constants.CHILD_TYPE_HEADER_ONLY -> {
                val data = (group as ExpandHeader).children[childIndex] as HeaderOnlyExpandChild
                (holder as HeaderOnlyChildViewHolder).bind(data)
            }
            Constants.CHILD_TYPE_HOME -> {
                val data = (group as ExpandHeader).children[childIndex] as HomeExpandChild
                (holder as HomeChildViewHolder).bind(data)
            }
            Constants.CHILD_TYPE_CUSTOMER -> {
                val data = (group as ExpandHeader).children[childIndex] as CustomerExpandChild
                (holder as CustomerChildViewHolder).bind(data, fragment)
            }
        }
    }

    override fun getChildViewType(position: Int, group: ExpandableGroup<*>?, childIndex: Int): Int {
        return (group as ExpandHeader).children[childIndex].itemType
    }

    override fun isChild(viewType: Int): Boolean {
        return viewType == Constants.CHILD_TYPE_CUSTOMER ||
                viewType == Constants.CHILD_TYPE_HEADER_ONLY ||
                viewType == Constants.CHILD_TYPE_HOME
    }
}