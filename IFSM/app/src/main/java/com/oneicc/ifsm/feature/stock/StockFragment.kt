package com.oneicc.ifsm.feature.stock

import android.widget.Toast
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.database.AppDatabase
import com.oneicc.ifsm.common.database.dao.StockDao
import com.oneicc.ifsm.common.model.Stock
import com.oneicc.ifsm.common.network.Api
import com.oneicc.ifsm.common.network.NetworkClient
import com.oneicc.ifsm.feature.stock.adapter.StockAdapter
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_stock.*
import timber.log.Timber

class StockFragment : BaseFragment(R.layout.fragment_stock) {
    private val data = ArrayList<Stock>()
    private lateinit var adapter: StockAdapter
    private lateinit var stockDao: StockDao

    override fun initData() {
        parentActivity?.setUpTitleAndBackAndRightToolbar(getString(R.string.stock_title)) {
            syncStockData()
        }
        stockDao = AppDatabase.getAppDataBase(context!!).stockDao()
        adapter = StockAdapter(data)

        loadData()
    }

    override fun initView() {
        rvStock.adapter = adapter
    }

    private fun loadData() {
        compositeDisposable.add(
                stockDao.getAll()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe({
                            data.clear()
                            data.addAll(it)
                            adapter.notifyDataSetChanged()
                            syncStockData()
                        }, {
                            Toast.makeText(context, it.message.toString(), Toast.LENGTH_SHORT).show()
                            Timber.e("loadData -> $it")
                        })
        )
    }

    private fun syncStockData() {
        compositeDisposable.add(
                NetworkClient.getRetrofit(context!!).create(Api::class.java).getAllStock()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            data.clear()
                            data.addAll(it)
                            adapter.notifyDataSetChanged()
                            saveNewStockData(it)
                            Toast.makeText(context, "Dữ liệu tồn kho đã được cập nhật!", Toast.LENGTH_LONG).show()
                        }, {
                            Toast.makeText(context, it.message.toString(), Toast.LENGTH_SHORT).show()
                            Timber.e("syncStockData -> $it")
                        })
        )
    }

    private fun saveNewStockData(data: List<Stock>) {
        compositeDisposable.add(Completable.fromAction { stockDao.insertAll(data) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    Timber.i("Save stock data success")
                }, {
                    Toast.makeText(context, it.message.toString(), Toast.LENGTH_SHORT).show()
                    Timber.e("saveNewStockData -> $it")
                }))
    }
}