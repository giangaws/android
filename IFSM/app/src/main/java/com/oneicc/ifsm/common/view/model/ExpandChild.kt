package com.oneicc.ifsm.common.view.model

import com.oneicc.ifsm.common.base.BaseModel
import com.oneicc.ifsm.common.util.Constants

open class ExpandChild(val title: String = "", val itemType: Int = Constants.CHILD_TYPE_HEADER_ONLY) : BaseModel()