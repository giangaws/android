package com.oneicc.ifsm.feature.kpi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseAdapter
import com.oneicc.ifsm.common.base.BaseViewHolder
import com.oneicc.ifsm.common.model.Revenue
import kotlinx.android.synthetic.main.item_revenue.view.*
import java.text.NumberFormat

class RevenueAdapter(data: ArrayList<Revenue>) : BaseAdapter<Revenue, RevenueAdapter.ViewHolder>(data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_revenue, parent, false))
    }

    class ViewHolder(view: View) : BaseViewHolder<Revenue>(view) {
        override fun bind(item: Revenue, position: Int) {
            itemView.tvCardCode.text = item.cardCode
            itemView.tvCardName.text = item.cardName
            itemView.tvTotalAmount.text =  NumberFormat.getInstance().format(item.totalAmount)
            itemView.tvLastOrderDate.text = "Đơn cuối: " + item.lastOrderDate
        }
    }
}