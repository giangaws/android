package com.oneicc.ifsm.feature.kpi

import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.model.Kpi
import com.oneicc.ifsm.common.model.Revenue
import com.oneicc.ifsm.feature.kpi.adapter.KpiAdapter
import com.oneicc.ifsm.feature.kpi.adapter.RevenueAdapter
import kotlinx.android.synthetic.main.fragment_kpi.*

class KpiFragment : BaseFragment(R.layout.fragment_kpi) {
    private var leftSelected = true
    private var activateColor: Int = Color.DKGRAY
    private var deactivateColor: Int = Color.LTGRAY

    private lateinit var kpiData: ArrayList<Kpi>
    private lateinit var kpiAdapter: KpiAdapter
    private lateinit var revenueData: ArrayList<Revenue>
    private lateinit var revenueAdapter: RevenueAdapter

    override fun initData() {
        parentActivity?.setUpTitleAndBackToolbar(getString(R.string.kpi_title))
        activateColor = ContextCompat.getColor(context!!, R.color.colorAccent)

        mock()
    }

    override fun initView() {
        tvKpi.text = kpiData.count { it.complete }.toString() + "/" + kpiData.size + "\nKPI"
        tvKpi.setOnClickListener {
            if (!leftSelected) {
                leftSelected = true
                setUpSelectedScreen()
            }
        }
        tvRevenue.text = revenueData.sumBy { it.totalAmount.toInt() }.toString().substring(0, 3) + " Tr\nDOANH SỐ"
        tvRevenue.setOnClickListener {
            if (leftSelected) {
                leftSelected = false
                setUpSelectedScreen()
            }
        }
        kpiAdapter = KpiAdapter(kpiData)
        revenueAdapter = RevenueAdapter(revenueData)
        rvData.addItemDecoration(DividerItemDecoration(rvData.context, DividerItemDecoration.VERTICAL))
        setUpSelectedScreen()
    }

    private fun mock() {
        kpiData = ArrayList(listOf(
                Kpi("DST72019", "KPI Doanh số bán hàng tháng 7/2019", 65000000.0, 30000000.0, false, "01/07/2019", "31/07/2019", 1166666.667),
                Kpi("SLQ32019", "KPI Sản lượng TP00008 - Pate Heo Nấm Đóng Hộp Tulip", 490.0, 505.0, true, "01/07/2019", "30/09/2019", 0.0),
                Kpi("SLQ32020", "KPI trưng bày", 10.0, 8.0, false, "01/07/2019", "30/09/2019", 0.021978022)
        ))
        revenueData = ArrayList(listOf(
                Revenue("KH00001", "Cửa hàng Hồng Phúc", 22090000.0, "05/07/2019"),
                Revenue("KH00002", "Cửa hàng Diễm Thái", 12150000.0, "06/07/2019"),
                Revenue("KH00003", "Cửa hàng Phúc Nhuận", 5490000.0, "07/07/2019"),
                Revenue("KH00004", "Cửa hàng Khánh Hòa", 34010000.0, "08/07/2019"),
                Revenue("KH00005", "Cửa hàng Vĩnh Khánh", 36045000.0, "09/07/2019"),
                Revenue("KH00006", "Cửa hàng Tô Châu", 2005000.0, "10/07/2019"),
                Revenue("KH00007", "Cửa hàng Tích Bàn", 3358000.0, "05/07/2019"),
                Revenue("KH00008", "Cửa hàng Phú Quý", 8000000.0, "06/07/2019"),
                Revenue("KH00009", "Nhà hàng Domina", 18900500.0, "07/07/2019"),
                Revenue("KH00010", "Nhà hàng Vince", 88903000.0, "08/07/2019"),
                Revenue("KH00011", "Nhà hàng Milan", 66490000.0, "09/07/2019"),
                Revenue("KH00012", "Nhà hàng Đông Hồ", 119670000.0, "10/07/2019"),
                Revenue("KH00013", "Nhà hàng Vườn Ẩm Thực", 7800000.0, "10/07/2019")
        ))
    }

    private fun setUpSelectedScreen() {
        if (leftSelected) {
            tvKpi.setTextColor(activateColor)
            vKpi.setBackgroundColor(activateColor)
            tvRevenue.setTextColor(deactivateColor)
            vRevenue.setBackgroundColor(deactivateColor)
            rvData.adapter = kpiAdapter
        } else {
            tvKpi.setTextColor(deactivateColor)
            vKpi.setBackgroundColor(deactivateColor)
            tvRevenue.setTextColor(activateColor)
            vRevenue.setBackgroundColor(activateColor)
            rvData.adapter = revenueAdapter
        }
    }
}