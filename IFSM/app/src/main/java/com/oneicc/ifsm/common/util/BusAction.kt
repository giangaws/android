package com.oneicc.ifsm.common.util

object BusAction {
    class RequestLocation
    class Location(val location: android.location.Location)
    class DoAction(val action: Int)
}