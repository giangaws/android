package com.oneicc.ifsm.common.base

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.util.NetworkUtils
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*

abstract class BaseActivity(private val layoutId: Int) : AppCompatActivity(), BaseView, FragmentView {
    var isTouchDisable: Boolean = false
    var isNetworkConnected: Boolean = false
    var disableDispatchTouchEvent: Boolean = false
    val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        isNetworkConnected = NetworkUtils.isNetworkConnected(applicationContext)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN && !disableDispatchTouchEvent) {
            val view = currentFocus
            if (view != null && (view is AppCompatEditText || view is EditText)) {
                val outRect = Rect()
                view.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    view.clearFocus()
                    val imm = (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }
            }
        }
        return isTouchDisable || super.dispatchTouchEvent(event)
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    fun hideKeyboard() {
        val view = currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showKeyboard() {
        val view = currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, 2)
        }
    }

    fun setFragment(fragment: BaseFragment, fromBottomAnimation: Boolean = false) {
        BaseFragment.addFragment(this, fragment, fromBottomAnimation)
    }

    fun getCurrentFragment(): BaseFragment? {
        return supportFragmentManager.findFragmentById(R.id.flContainer) as BaseFragment
    }

    fun getView(): BaseView = this

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            getCurrentFragment()?.pop()
        } else {
            finish()
        }
    }

    private fun defaultBack(): Boolean {
        if (supportFragmentManager.backStackEntryCount <= 1) {
            finish()
            return true
        }
        return false
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}