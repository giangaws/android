package com.oneicc.ifsm.common.network

data class BaseResponse(var title: String? = null, var message: String? = null, var code: String? = null)