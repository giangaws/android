package com.oneicc.ifsm.feature.user

import android.annotation.SuppressLint
import android.widget.Toast
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.database.AppDatabase
import com.oneicc.ifsm.common.database.dao.CustomerDao
import com.oneicc.ifsm.common.model.Customer
import com.oneicc.ifsm.common.model.Item
import com.oneicc.ifsm.common.model.Stock
import com.oneicc.ifsm.feature.home.HomeFragment
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*
import timber.log.Timber

class LoginFragment : BaseFragment(R.layout.fragment_login) {
    private lateinit var customerDao: CustomerDao

    override fun initData() {
        parentActivity?.setUpTitleToolbar()
        customerDao = AppDatabase.getAppDataBase(context!!).customerDao()
        compositeDisposable.add(
            customerDao.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    if (it.isNotEmpty()) {
                        val customerList = it
                        btnLogin.setOnClickListener {
                            login(customerList)
                        }
                    } else {
                        Timber.e("setUpData -> empty data")
                        createCustomerData()
                    }
                }, {
                    Timber.e("setUpData -> $it")
                    createCustomerData()
                })
        )
    }

    override fun initView() {
        btnLogin.setOnClickListener {
            Toast.makeText(context, "Đang nạp dữ liệu, xin hãy thử lại sau vài giây nữa!", Toast.LENGTH_LONG).show()
        }
    }

    private fun createCustomerData() {
        val customerList = ArrayList<Customer>()
        customerList.add(
            Customer(
                "Cửa hàng Hồng Phúc",
                "KH00001",
                "285 Cach Mang Thang Tam",
                "Anh Phúc",
                "0935550101",
                106.6904559,
                10.7728832,
                "https://www.kiotviet.vn/wp-content/uploads/2017/12/3-kho-khan-thuong-gap-khi-mo-cua-hang-tap-hoa-o-nong-thon-1.jpg"
            )
        )
        customerList.add(
            Customer(
                "Cửa hàng Diễm Thái",
                "KH00002",
                "286 Cach Mang Thang Tam",
                "Anh Thái",
                "0935550102",
                106.6904559,
                10.7828842,
                "https://www.kiotviet.vn/wp-content/uploads/2014/07/tap-hoa.jpg"
            )
        )
        customerList.add(
            Customer(
                "Cửa hàng Phúc Nhuận",
                "KH00003",
                "287 Cach Mang Thang Tam",
                "Chị Nhuận",
                "0935550103",
                106.6954559,
                10.7821842,
                "https://www.sapo.vn/blog/wp-content/uploads/2019/02/kinh-nghiem-mo-cua-hang-tap-hoa-1.png"
            )
        )
        customerList.add(
            Customer(
                "Cửa hàng Khánh Hòa",
                "KH00004",
                "288 Cach Mang Thang Tam",
                "Chị Hòa",
                "0935550104",
                106.6404559,
                10.7825842,
                "https://vinatechjsc.vn/wp-content/uploads/2018/09/kệ-tạp-hóa-tôn-đục-lỗ.jpg"
            )
        )
        customerList.add(
            Customer(
                "Cửa hàng Vĩnh Khánh",
                "KH00005",
                "289 Cach Mang Thang Tam",
                "Anh Khánh",
                "0932342340",
                106.6906559,
                10.8828842,
                "https://nhatminh.net/wp-content/uploads/2017/02/cua-hang-tap-hoa.jpg"
            )
        )
        customerList.add(
            Customer(
                "Cửa hàng Tô Châu",
                "KH00006",
                "290 Cach Mang Thang Tam",
                "Chị Châu",
                "0932342341",
                106.6704559,
                10.7828342,
                "https://onetechgroup.com.vn/wp-content/uploads/2019/04/mo-cua-hang-tap-hoa.jpg"
            )
        )
        customerList.add(
            Customer(
                "Cửa hàng Tích Bàn",
                "KH00007",
                "290 Cach Mang Thang Tam",
                "Anh Bàn",
                "0932342342",
                106.6944559,
                10.7128842,
                "http://bienquangcaodep247.com/wp-content/uploads/2018/12/list-nhung-mau-bien-quang-cao-cua-hang-tap-hoa-ban-khong-the-ngo-lo-2-1.png"
            )
        )
        customerList.add(
            Customer(
                "Cửa hàng Phú Quý",
                "KH00008",
                "210 Cach Mang Thang Tam",
                "Anh Quý",
                "0932342343",
                106.1904559,
                10.7827842,
                "https://vinaad.vn/wp-content/uploads/2018/07/bien-quang-cao-cua-hang-2.jpg"
            )
        )
        customerList.add(
            Customer(
                "Nhà hàng Domina",
                "KH00009",
                "2 Ngô Quyền",
                "Chị Na",
                "0932342343",
                106.6944559,
                10.7858842,
                "https://saohanoi.vn/wp-content/uploads/2018/12/mau-bang-hieu-cua-hang-tap-hoa-2.jpg"
            )
        )
        customerList.add(
            Customer(
                "Nhà hàng Vince",
                "KH00010",
                "112 Ngô Quyền",
                "Chị Vin",
                "0935550306",
                106.6904659,
                10.7838842,
                "https://cdn.vietnambiz.vn/stores/news_dataimages/hanhtt/072017/13/22/2125_tap-hoa.jpg",
                outRoute = true
            )
        )
        customerList.add(
            Customer(
                "Nhà hàng Milan",
                "KH00011",
                "112 Tố Hữu",
                "Chị Lan",
                "0935550309",
                106.6914659,
                11.7838842,
                "https://open24.vn/Img_NoiDungBaiViet/20180319/images/open24%202.jpg",
                outRoute = true
            )
        )
        customerList.add(
            Customer(
                "Nhà hàng Đông Hồ",
                "KH00012",
                "112 Đông Hồ",
                "Anh Hồ",
                "0935550001",
                106.6924659,
                10.7438842,
                "https://lh4.googleusercontent.com/G6R4Ssr0MfEE_8lPREzX4mfIwoMYb0Ul75b9X7lwHuHwD5nKiR2ak-tXh_yE8ozFilVxv1H7XqSGVE3uPjJacAFSC0vwF1K2sjRVFUnueeCyP4oluT_EfT4UJVJ8KAWcA6ZTfLjL",
                outRoute = true
            )
        )
        customerList.add(
            Customer(
                "Nhà hàng Vườn Ẩm Thực",
                "KH00013",
                "12 Phan Văn Trị",
                "Anh Phú",
                "0935550099",
                106.6954659,
                10.7868842,
                "http://sieuthimasomavach.vn/upload/misc/lawson-tokyo-convenience-store_1418441884.jpg",
                outRoute = true
            )
        )
        compositeDisposable.add(Completable.fromAction { customerDao.insertAll(customerList) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({
                createItemData()
            }, {
                Timber.e("createCustomerData -> $it")
            }))
    }

    private fun createItemData() {
        val itemList = ArrayList<Item>()
        itemList.add(Item("BK00001", "Banh dua nuong", true))
        itemList.add(Item("BK00002", "Keo deo Chupa Chups", false))
        itemList.add(Item("BK00003", "Socola vien Milo", true))
        itemList.add(Item("BK00004", "Banh gao One", true))
        itemList.add(Item("BK00005", "Keo trai cay Fruit Candy", true))
        itemList.add(Item("GK00001", "Nuoc suoi Vinh Hao", false))
        itemList.add(Item("GK00002", "Nuoc ngot Coca Cola", true))
        itemList.add(Item("GK00003", "Nuoc ngot Pepsi", true))
        itemList.add(Item("GK00004", "Bia Budweiser Alumium", true))
        itemList.add(Item("GK00005", "Nước Uống Thể Thao Aquarius", false))
        itemList.add(Item("GK00006", "Nước Giải Khát Sting Đỏ Dâu", false))
        itemList.add(Item("TP00001", "Mi ly khoai tay Cung Dinh", true))
        itemList.add(Item("TP00002", "Mi goi Hao Hao tom chua cay", true))
        itemList.add(Item("TP00003", "Mi goi Omachi Business Class", false))
        itemList.add(Item("TP00004", "Mì Shin Cay Hàn Quốc Nong Shim", false))
        itemList.add(Item("TP00005", "Cháo Bí Ngô Mật Ong Hàn Quốc", true))
        itemList.add(Item("TP00006", "Đậu Phộng Rang Tỏi Ớt", false))
        itemList.add(Item("TP00007", "Rong Biển Sấy", true))
        itemList.add(Item("TP00008", "Pate Heo Nấm Đóng Hộp Tulip", false))
        compositeDisposable.add(Completable.fromAction {
            AppDatabase.getAppDataBase(context!!).itemDao().insertAll(itemList)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({
                createStockData()
            }, {
                Timber.e("createItemData -> $it")
            }))
    }

    private fun createStockData() {
        val stockList = ArrayList<Stock>()
        stockList.add(Stock("BK00001", "Banh dua nuong", "10", "MiniMart", -24, 21200.0))
        stockList.add(Stock("BK00002", "Keo deo Chupa Chups", "10", "MiniMart", -5, 18700.0))
        stockList.add(Stock("BK00003", "Socola vien Milo", "10", "MiniMart", 0, 31760.0))
        stockList.add(Stock("BK00004", "Banh gao One", "10", "MiniMart", -4, 34500.0))
        stockList.add(Stock("BK00005", "Keo trai cay Fruit Candy", "10", "MiniMart", 0, 16800.0))
        stockList.add(Stock("CB00001", "Xúc xích Đức", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("CB00002", "Chả cá rong biển", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("CB00003", "Ba rọi xông khói", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("CB00004", "Xúc xích tươi vị phô mai", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("CB00005", "Lạp xưởng Mai Quế Lộ", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("CB00006", "Gan ngỗng", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("CB00007", "Burger bò", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("CB00008", "Cá ngừ ngâm dầu", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("GK00001", "Nuoc suoi Vinh Hao", "10", "MiniMart", 1488, 5400.0))
        stockList.add(Stock("GK00002", "Nuoc ngot Coca Cola", "10", "MiniMart", 2499, 14300.0))
        stockList.add(Stock("GK00003", "Nuoc ngot Pepsi", "10", "MiniMart", 1900, 13400.0))
        stockList.add(Stock("GK00004", "Bia Budweiser Alumium", "10", "MiniMart", 450, 17300.0))
        stockList.add(Stock("GK00005", "Nước Uống Thể Thao Aquarius", "10", "MiniMart", 797, 41000.0))
        stockList.add(Stock("GK00006", "Nước Giải Khát Sting Đỏ Dâu", "10", "MiniMart", 1669, 48000.0))
        stockList.add(Stock("TB00001", "Thịt bò mềm Argentina", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TB00002", "Thịt mông bò Mỹ", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TB00003", "Lõi sườn bò Mỹ", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TB00004", "Phi lê bò Mỹ", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TB00005", "Ba chỉ bò Mỹ", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TP00001", "Mi ly khoai tay Cung Dinh", "10", "MiniMart", 2395, 8000.0))
        stockList.add(Stock("TP00002", "Mi goi Hao Hao tom chua cay", "10", "MiniMart", 3300, 9000.0))
        stockList.add(Stock("TP00003", "Mi goi Omachi Business Class", "10", "MiniMart", 900, 25000.0))
        stockList.add(Stock("TP00004", "Mì Shin Cay Hàn Quốc Nong Shim", "10", "MiniMart", 399, 59000.0))
        stockList.add(Stock("TP00005", "Cháo Bí Ngô Mật Ong Hàn Quốc", "10", "MiniMart", 380, 72600.0))
        stockList.add(Stock("TP00006", "Đậu Phộng Rang Tỏi Ớt", "10", "MiniMart", 787, 33000.0))
        stockList.add(Stock("TP00007", "Rong Biển Sấy", "10", "MiniMart", 750, 18000.0))
        stockList.add(Stock("TP00008", "Pate Heo Nấm Đóng Hộp Tulip", "10", "MiniMart", 300, 48000.0))
        stockList.add(Stock("TS00001", "Cá thu", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TS00002", "Tôm sú", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TS00003", "Cá nục suông", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TS00004", "Phi lê cá hồi", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TS00005", "Mực ống nguyên con", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TS00006", "Cá chim biển trắng", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TS00007", "Phi lê cá lăng đại dương", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("TS00008", "Còi sò điệp", "10", "MiniMart", 0, 0.0))
        stockList.add(Stock("BK00001", "Banh dua nuong", "20", "Restaurant", 0, 21200.0))
        stockList.add(Stock("BK00002", "Keo deo Chupa Chups", "20", "Restaurant", 0, 18700.0))
        stockList.add(Stock("BK00003", "Socola vien Milo", "20", "Restaurant", 0, 31760.0))
        stockList.add(Stock("BK00004", "Banh gao One", "20", "Restaurant", 0, 34500.0))
        stockList.add(Stock("BK00005", "Keo trai cay Fruit Candy", "20", "Restaurant", 0, 16800.0))
        stockList.add(Stock("CB00001", "Xúc xích Đức", "20", "Restaurant", 700, 0.0))
        stockList.add(Stock("CB00002", "Chả cá rong biển", "20", "Restaurant", 400, 0.0))
        stockList.add(Stock("CB00003", "Ba rọi xông khói", "20", "Restaurant", 15600, 0.0))
        stockList.add(Stock("CB00004", "Xúc xích tươi vị phô mai", "20", "Restaurant", 17600, 0.0))
        stockList.add(Stock("CB00005", "Lạp xưởng Mai Quế Lộ", "20", "Restaurant", 19000, 0.0))
        stockList.add(Stock("CB00006", "Gan ngỗng", "20", "Restaurant", 2000, 0.0))
        stockList.add(Stock("CB00007", "Burger bò", "20", "Restaurant", 4000, 0.0))
        stockList.add(Stock("CB00008", "Cá ngừ ngâm dầu", "20", "Restaurant", 3000, 0.0))
        stockList.add(Stock("TB00001", "Thịt bò mềm Argentina", "20", "Restaurant", 45900, 0.0))
        stockList.add(Stock("TB00002", "Thịt mông bò Mỹ", "20", "Restaurant", 23000, 0.0))
        stockList.add(Stock("TB00003", "Lõi sườn bò Mỹ", "20", "Restaurant", 47000, 0.0))
        stockList.add(Stock("TB00004", "Phi lê bò Mỹ", "20", "Restaurant", 34900, 0.0))
        stockList.add(Stock("TB00005", "Ba chỉ bò Mỹ", "20", "Restaurant", 63000, 0.0))
        stockList.add(Stock("TP00007", "Rong Biển Sấy", "20", "Restaurant", 0, 18000.0))
        stockList.add(Stock("TP00008", "Pate Heo Nấm Đóng Hộp Tulip", "20", "Restaurant", 0, 48000.0))
        stockList.add(Stock("TS00001", "Cá thu", "20", "Restaurant", 8700, 0.0))
        stockList.add(Stock("TS00002", "Tôm sú", "20", "Restaurant", 0, 0.0))
        stockList.add(Stock("TS00003", "Cá nục suông", "20", "Restaurant", 12000, 0.0))
        stockList.add(Stock("TS00004", "Phi lê cá hồi", "20", "Restaurant", 4900, 0.0))
        stockList.add(Stock("TS00005", "Mực ống nguyên con", "20", "Restaurant", 6500, 0.0))
        stockList.add(Stock("TS00006", "Cá chim biển trắng", "20", "Restaurant", 9800, 0.0))
        stockList.add(Stock("TS00007", "Phi lê cá lăng đại dương", "20", "Restaurant", 12000, 0.0))
        stockList.add(Stock("TS00008", "Còi sò điệp", "20", "Restaurant", 4500, 0.0))

        compositeDisposable.add(Completable.fromAction {
            AppDatabase.getAppDataBase(context!!).stockDao().insertAll(stockList)
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({
                btnLogin.setOnClickListener {
                    getCustomerData()
                }
            }, {
                Timber.e("createStockData -> $it")
            }))
    }

    private fun getCustomerData() {
        val customerDao = AppDatabase.getAppDataBase(context!!).customerDao()
        compositeDisposable.add(
            customerDao.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    login(it)
                }, {
                    Timber.e("getCustomerData -> $it")
                })
        )
    }

    @SuppressLint("MissingPermission")
    private fun login(customerList: List<Customer>) {
        setFragment(fragmentManager!!, HomeFragment.getInstance(customerList.toTypedArray()))
    }
}