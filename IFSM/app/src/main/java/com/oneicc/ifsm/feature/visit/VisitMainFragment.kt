package com.oneicc.ifsm.feature.visit

import android.util.Log
import androidx.recyclerview.widget.DividerItemDecoration
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.oneicc.ifsm.R
import com.oneicc.ifsm.app.MainActivity
import com.oneicc.ifsm.common.activity.ActivityModel
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.database.AppDatabase
import com.oneicc.ifsm.common.model.Customer
import com.oneicc.ifsm.common.util.BusAction
import com.oneicc.ifsm.common.util.Constants
import com.oneicc.ifsm.feature.order.OrderFragment
import com.oneicc.ifsm.feature.stock.StockFragment
import com.oneicc.ifsm.feature.visit.adapter.ActivityAdapter
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_visit_main.*
import org.greenrobot.eventbus.EventBus
import java.text.NumberFormat

class VisitMainFragment : BaseFragment(R.layout.fragment_visit_main) {
    private lateinit var customer: Customer
    private lateinit var adapter: ActivityAdapter

    override fun initData() {
        customer = MainActivity.currentCustomer!!
        val data = ArrayList(
                listOf(
                        ActivityModel(R.drawable.ic_camera_line, "Chụp ảnh đại diện"),
                        ActivityModel(R.drawable.ic_box_line, "Tồn kho", {
                            addFragment(StockFragment())
                        }),
                        ActivityModel(R.drawable.ic_percent_line, "Tham gia chương trình TMK"),
                        ActivityModel(R.drawable.ic_presentation_line, "Tài liệu bán hàng"),
                        ActivityModel(R.drawable.ic_order_line, "Đặt hàng", {
                            EventBus.getDefault().post(BusAction.DoAction(Constants.ACTION_START_ORDER))
                            addFragment(OrderFragment())
                        }),
                        ActivityModel(R.drawable.ic_images_line, "Chụp ảnh trưng bày"),
                        ActivityModel(R.drawable.ic_comment_line, "Ý kiến khách hàng")
                )
        )
        adapter = ActivityAdapter(data)
    }

    override fun initView() {
        rvMenu.addItemDecoration(DividerItemDecoration(rvMenu.context, DividerItemDecoration.VERTICAL))
        rvMenu.adapter = adapter

        Glide.with(imgCustomer).load(customer.pictureUrl)
                .centerInside()
                .override(Constants.DEFAULT_HOME_ICON_SIZE, Constants.DEFAULT_HOME_ICON_SIZE)
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.default_avatar)
                .into(imgCustomer)

        tvCustomerName.text = customer.customerName
        tvContact.text = customer.contact
        tvAddress.text = customer.address
        tvPhone.text = customer.phone
        tvLastMonthRevenue.text = NumberFormat.getInstance().format(customer.lastMonthRevenue)
        tvRevenue.text = NumberFormat.getInstance().format(customer.revenue)
        tvDebt.text = NumberFormat.getInstance().format(customer.debt)
        btnEndVisit.setOnClickListener {
            if (MainActivity.currentDocId.isNullOrEmpty()) {
                customer.visitStatus = Constants.VISIT_STATUS_NO_ORDER
            } else {
                customer.visitStatus = Constants.VISIT_STATUS_HAS_ORDER
            }
            compositeDisposable.add(Completable.fromAction {
                AppDatabase.getAppDataBase(context!!).customerDao().update(customer)
            }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe({
                        EventBus.getDefault().post(BusAction.DoAction(Constants.ACTION_END_VISIT))
                        (parentFragment as VisitFragment).pop()
                    }, {
                        Log.e("VisitMainFragment", "End Visit -> $it")
                    }))
        }
    }
}