package com.oneicc.ifsm.common.util

import android.animation.ObjectAnimator
import android.view.View

object AnimUtils {
    private const val ANIMATION_DURATION = 300L

    fun expandAnimation(view: View) {
        val animator = ObjectAnimator.ofFloat(view, View.ROTATION, 0f, 180f)
        animator.duration = ANIMATION_DURATION
        animator.start()
    }

    fun collapseAnimation(view: View) {
        val animator = ObjectAnimator.ofFloat(view, View.ROTATION, 180f, 360f)
        animator.duration = ANIMATION_DURATION
        animator.start()
    }

    fun blinkAnimation(view: View) {
        val animator = ObjectAnimator.ofFloat(view, View.ALPHA, 0f, 1f)
        animator.duration = 200L
        animator.repeatCount = 3
        animator.start()
    }
}