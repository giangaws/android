package com.oneicc.ifsm.common.base

interface BaseDatabaseView<T> : BaseView {
    fun onLoadDataSuccess(data: T)
    fun onLoadDateFailure(throwable: Throwable)
}