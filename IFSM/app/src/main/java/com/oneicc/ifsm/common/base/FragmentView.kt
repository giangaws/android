package com.oneicc.ifsm.common.base

interface FragmentView {
    fun onFragmentAttached()
    fun onFragmentDetached(tag: String)
}