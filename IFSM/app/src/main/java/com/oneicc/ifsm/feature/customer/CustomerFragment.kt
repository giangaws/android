package com.oneicc.ifsm.feature.customer

import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.database.AppDatabase
import com.oneicc.ifsm.common.model.Customer
import com.oneicc.ifsm.feature.customer.adapter.CustomerAdapter
import kotlinx.android.synthetic.main.fragment_customer.*

class CustomerFragment : BaseFragment(R.layout.fragment_customer), CustomerContract.View {
    private lateinit var presenter: CustomerPresenter
    private lateinit var adapter: CustomerAdapter
    private val data = ArrayList<Customer>()

    override fun initData() {
        presenter = CustomerPresenter(this, AppDatabase.getAppDataBase(context!!))
        parentActivity?.setUpTitleAndBackToolbar(getString(R.string.customer_list))
        adapter = CustomerAdapter(data)
        presenter.loadData()
    }

    override fun initView() {
        rvCustomer.addItemDecoration(DividerItemDecoration(rvCustomer.context, DividerItemDecoration.VERTICAL))
        rvCustomer.adapter = adapter
    }

    override fun onLoadDataSuccess(data: List<Customer>) {
        this.data.clear()
        this.data.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun onLoadDateFailure(throwable: Throwable) {
        Toast.makeText(context!!, throwable.toString(), Toast.LENGTH_SHORT).show()
    }
}