package com.oneicc.ifsm.feature.home.viewholder

import android.view.View
import android.widget.TextView
import com.oneicc.ifsm.R
import com.oneicc.ifsm.feature.home.model.HeaderOnlyExpandChild
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder

class HeaderOnlyChildViewHolder(view: View) : ChildViewHolder(view) {
    private val tvChildTitle: TextView = itemView.findViewById(R.id.tvChildTitle)

    fun bind(data: HeaderOnlyExpandChild) {
        tvChildTitle.text = data.title
    }
}