package com.oneicc.ifsm.common.database

import androidx.room.TypeConverter
import com.oneicc.ifsm.common.util.DateTimeUtils
import java.util.*

class Converters {
    @TypeConverter
    fun fromString(value: String?): Date? {
        return if (value == null) null else DateTimeUtils.convertFromSQLFormatToDate(value)
    }

    @TypeConverter
    fun dateToString(date: Date?): String? {
        return if (date == null) null else DateTimeUtils.convertToSOPostFormatString(date)
    }
}