package com.oneicc.ifsm.common.model

import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.oneicc.ifsm.common.base.BaseModel

@Entity(primaryKeys = ["itemCode", "whsCode"])
data class Stock(
    @Expose
    @SerializedName("ItemCode")
    val itemCode: String = "",
    @Expose
    @SerializedName("ItemName")
    val itemName: String = "",
    @Expose
    @SerializedName("WhsCode")
    val whsCode: String = "",
    @Expose
    @SerializedName("WhsName")
    val whsName: String = "",
    @Expose
    @SerializedName("InStock")
    var inStock: Int = 0,
    @Expose
    @SerializedName("Price")
    var price: Double = 0.0
) : BaseModel()