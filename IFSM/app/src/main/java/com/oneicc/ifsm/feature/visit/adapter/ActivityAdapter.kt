package com.oneicc.ifsm.feature.visit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.activity.ActivityModel
import com.oneicc.ifsm.common.base.BaseAdapter
import com.oneicc.ifsm.common.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_activity.view.*

class ActivityAdapter(data: ArrayList<ActivityModel>) : BaseAdapter<ActivityModel, ActivityAdapter.ViewHolder>(data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_activity, parent, false))
    }

    class ViewHolder(view: View) : BaseViewHolder<ActivityModel>(view) {
        override fun bind(item: ActivityModel, position: Int) {
            if (item.icon != -1) {
                itemView.imgActivityIcon.setImageResource(item.icon)
            }
            itemView.tvActivityName.text = item.name
            itemView.vActivity.setOnClickListener {
                item.onClick.invoke()
            }
        }
    }
}