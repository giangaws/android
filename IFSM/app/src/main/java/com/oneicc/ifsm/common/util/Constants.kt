package com.oneicc.ifsm.common.util

import com.oneicc.ifsm.BuildConfig
import com.oneicc.ifsm.R

object Constants {
    const val REGEX_NUMBER = "^[0-9]*$"
    const val REGEX_ALPHA_NUMBER = "^[a-zA-Z0-9]*$"
    const val REGEX_ALPHA_NUMBER_PLUS = "^[a-zA-Z0-9 \\r\\n,.]*$"


    const val ANDROID_CHANNEL_ID = BuildConfig.APPLICATION_ID
    const val ANDROID_CHANNEL_NAME = "Pets"

    const val DATABASE_NAME = "iSalesMobile"

    var STATE_FOREGROUND = false

    const val DEFAULT_ICON = R.drawable.ic_arrow_right

    const val HTTP_RESPONSE_200 = 200
    const val HTTP_RESPONSE_400 = 400
    const val HTTP_RESPONSE_401 = 401
    const val HTTP_RESPONSE_404 = 404

    const val CONNECTION_TIMEOUT_SECOND = 120L

    const val CHILD_TYPE_HEADER_ONLY = 3
    const val CHILD_TYPE_HOME = 4
    const val CHILD_TYPE_CUSTOMER = 5

    const val VISIT_STATUS_HAS_ORDER = 1
    const val VISIT_STATUS_NO_ORDER = 2
    const val VISIT_STATUS_CANNOT_VISIT = 3
    const val VISIT_STATUS_NOT_YET = 4

    const val DEFAULT_HOME_ICON_SIZE = 150

    const val ACTION_NON = 0
    const val ACTION_START_VISIT = 1
    const val ACTION_END_VISIT = 2
    const val ACTION_START_ORDER = 3
    const val ACTION_END_ORDER = 4

    const val ACTIVITY_CODE_VISIT = "A001"
    const val ACTIVITY_CODE_ORDER = "A006"

    const val ACCESS_TOKEN = "hKY2Tcauz9+/+H5NUw5OOkVjzh5vXLddEAeSbvjxrSSu4jCCypvYAZGE4HCmGPWVsEDrP6RsidI="
}