package com.oneicc.ifsm.common.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.oneicc.ifsm.common.database.dao.*
import com.oneicc.ifsm.common.model.*
import com.oneicc.ifsm.common.util.Constants

@Database(
    entities = [Customer::class, SOHeader::class, SOLine::class, Item::class, Stock::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun customerDao(): CustomerDao
    abstract fun soHeaderDao(): SOHeaderDao
    abstract fun soLineDao(): SOLineDao
    abstract fun stockDao(): StockDao
    abstract fun itemDao(): ItemDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        Constants.DATABASE_NAME
                    ).build()
                }
            }
            return INSTANCE!!
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }
}