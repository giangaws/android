package com.oneicc.ifsm.feature.kpi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseAdapter
import com.oneicc.ifsm.common.base.BaseViewHolder
import com.oneicc.ifsm.common.model.Kpi
import kotlinx.android.synthetic.main.item_kpi.view.*
import java.text.NumberFormat

class KpiAdapter(data: ArrayList<Kpi>) : BaseAdapter<Kpi, KpiAdapter.ViewHolder>(data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_kpi, parent, false))
    }

    class ViewHolder(view: View) : BaseViewHolder<Kpi>(view) {
        override fun bind(item: Kpi, position: Int) {
            itemView.tvKpiName.text = item.kpiName
            itemView.tvProcess.text = "Đạt: " + NumberFormat.getInstance().format(item.current) +
                    " / " + NumberFormat.getInstance().format(item.target)
            itemView.tvTarget.text = "Mục tiêu ngày: " + NumberFormat.getInstance().format(item.todayTarget)
            itemView.tvDate.text = item.startDate + " - " + item.endDate
        }
    }
}