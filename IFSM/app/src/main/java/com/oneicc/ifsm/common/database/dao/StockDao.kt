package com.oneicc.ifsm.common.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.oneicc.ifsm.common.base.BaseDao
import com.oneicc.ifsm.common.model.Stock
import io.reactivex.Single

@Dao
interface StockDao : BaseDao<Stock> {
    @Query("SELECT * From Stock")
    fun getAll(): Single<List<Stock>>
}