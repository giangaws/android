package com.oneicc.ifsm.common.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.oneicc.ifsm.common.base.BaseDao
import com.oneicc.ifsm.common.model.SOLine
import io.reactivex.Single

@Dao
interface SOLineDao : BaseDao<SOLine> {
    @Query("SELECT * FROM SOLine")
    fun getAll(): Single<List<SOLine>>
}