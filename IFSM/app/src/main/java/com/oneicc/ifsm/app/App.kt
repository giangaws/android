package com.oneicc.ifsm.app

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.oneicc.ifsm.common.util.Constants
import com.oneicc.ifsm.common.util.DateTimeUtils
import timber.log.Timber
import java.util.*

class App : Application(), LifecycleObserver {
    private object Holder {
        val INSTANCE = App()
    }

    companion object {
        val instance: App by lazy { Holder.INSTANCE }
    }

    override fun onCreate() {
        super.onCreate()
        DateTimeUtils.DEFAULT_LOCALE = Locale.getDefault()
        setupLifecycleListener()
    }

    private fun setupLifecycleListener() {
        ProcessLifecycleOwner.get().lifecycle
            .addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {
        Timber.d("Returning to foreground…")
        Constants.STATE_FOREGROUND = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() {
        Timber.d("Moving to background…")
        Constants.STATE_FOREGROUND = false
    }
}