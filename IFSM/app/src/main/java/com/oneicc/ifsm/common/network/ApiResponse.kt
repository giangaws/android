package com.oneicc.ifsm.common.network

import com.google.gson.annotations.Expose

data class ApiResponse<T>(
    @Expose
    val data: T,
    @Expose
    val result: Int,
    @Expose
    val errorMessage: String?
)