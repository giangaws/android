package com.oneicc.ifsm.common.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.oneicc.ifsm.common.base.BaseDao
import com.oneicc.ifsm.common.model.SOHeader
import io.reactivex.Single

@Dao
interface SOHeaderDao : BaseDao<SOHeader> {
    @Query("SELECT * FROM SOHeader")
    fun getAll(): Single<List<SOHeader>>
}