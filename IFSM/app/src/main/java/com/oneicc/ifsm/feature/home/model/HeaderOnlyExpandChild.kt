package com.oneicc.ifsm.feature.home.model

import com.oneicc.ifsm.common.util.Constants
import com.oneicc.ifsm.common.view.model.ExpandChild

data class HeaderOnlyExpandChild(val headerTitle: String) : ExpandChild(headerTitle, Constants.CHILD_TYPE_HEADER_ONLY)