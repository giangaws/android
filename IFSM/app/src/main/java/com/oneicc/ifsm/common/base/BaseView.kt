package com.oneicc.ifsm.common.base

interface BaseView {
    fun showLoading()
    fun hideLoading()
}