package com.oneicc.ifsm.feature.order.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseAdapter
import com.oneicc.ifsm.common.base.BaseViewHolder
import com.oneicc.ifsm.common.model.SOLine
import kotlinx.android.synthetic.main.item_order_confirm.view.*
import java.text.NumberFormat

class OrderConfirmAdapter(data: ArrayList<SOLine>) : BaseAdapter<SOLine, OrderConfirmAdapter.ViewHolder>(data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_order_confirm, parent, false))
    }

    class ViewHolder(view: View) : BaseViewHolder<SOLine>(view) {
        override fun bind(item: SOLine, position: Int) {
            itemView.tvItemName.text = item.itemName
            itemView.tvItemCode.text = item.itemCode
            itemView.tvQuantity.text = item.quantity.toString()
            itemView.tvTotal.text = NumberFormat.getInstance().format(item.lineTotal)
        }
    }
}