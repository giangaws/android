package com.oneicc.ifsm.feature.home.viewholder

import android.view.View
import androidx.core.content.ContextCompat
import com.oneicc.ifsm.R
import com.oneicc.ifsm.feature.home.model.HomeExpandChild
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import kotlinx.android.synthetic.main.item_expand_child_home.view.*

class HomeChildViewHolder(view: View) : ChildViewHolder(view) {
    fun bind(data: HomeExpandChild) {
        itemView.vStatus.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorVisitStatusNotYet))
        itemView.imgChildIcon.setImageDrawable(ContextCompat.getDrawable(itemView.context, data.icon))
        itemView.tvChildTitle.text = data.title
        itemView.tvChildDetail.text = data.description
        itemView.imgChildIcon.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorAccent))
        itemView.imgChildIcon.setImageResource(data.icon)
        itemView.vHomeExpandChild.setOnClickListener {
            data.onClick.invoke()
        }
    }
}