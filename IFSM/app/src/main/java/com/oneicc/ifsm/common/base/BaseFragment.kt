package com.oneicc.ifsm.common.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.oneicc.ifsm.R
import com.oneicc.ifsm.app.MainActivity
import com.oneicc.ifsm.feature.home.HomeFragment
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment(private val layoutId: Int) : Fragment(), BaseView {
    private var isNetworkConnected = false
    private var currentFragmentLevel = 1
    var parentActivity: MainActivity? = null
    protected val compositeDisposable = CompositeDisposable()

    protected abstract fun initData()
    protected abstract fun initView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) {
            val activity = context as MainActivity?
            this.parentActivity = activity
            activity!!.onFragmentAttached()
            isNetworkConnected = parentActivity != null && parentActivity!!.isNetworkConnected
        }
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        if (disableFragmentAnimation) {
            val a = object : Animation() {}
            a.duration = 0
            return a
        }
        return super.onCreateAnimation(transit, enter, nextAnim)
    }

    override fun onDetach() {
        parentActivity = null
        super.onDetach()
    }

    override fun showLoading() {
        parentActivity?.showLoading()
    }

    override fun hideLoading() {
        parentActivity?.hideLoading()
    }

    /**
     * Add fragment with higher level
     * @param fromBottomAnimation default is FALSE: fragment will appear from right to left; TRUE: fragment will appear from bottom to top
     */
    fun addFragment(fragment: BaseFragment, fromBottomAnimation: Boolean = false) {
        addFragment(activity, fragment, fromBottomAnimation)
    }

    fun setFragmentChild(fragment: BaseFragment, containerViewId: Int) {
        childFragmentManager
                .beginTransaction()
                .replace(containerViewId, fragment)
                .addToBackStack(fragment.getTransactionName())
                .commit()
    }

    fun clearBackStackChild() {
        if (childFragmentManager.backStackEntryCount > 0) {
            childFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    fun setFragmentChildNotAnimation(fragment: BaseFragment, containerViewId: Int) {
        setFragment(childFragmentManager, fragment, containerViewId, -1, -1, -1, -1)
    }

    fun pop() {
        if (activity == null) {
            return
        }
        popBackStack(activity!!.supportFragmentManager, this)
        isPopBackStack = false
    }

    fun getFragmentLevel(): Int {
        return currentFragmentLevel
    }

    fun setFragmentLevel(fragmentLevel: Int) {
        this.currentFragmentLevel = fragmentLevel
    }

    private fun getTransactionName(): String {
        return "Level$currentFragmentLevel"
    }

    fun popTo(cls: Class<*> = HomeFragment::class.java, disableAnimation: Boolean = false) {
        if (activity == null) {
            return
        }
        val fragmentManager = activity!!.supportFragmentManager
        var currentFragment = fragmentManager.findFragmentById(R.id.flContainer)
        while (!cls.isInstance(currentFragment)) {
            popBackStack(fragmentManager, currentFragment as BaseFragment, disableAnimation)
            isPopBackStack = false
            currentFragment = fragmentManager.findFragmentById(R.id.flContainer)
        }
    }

    fun showKeyboard() {
        parentActivity?.showKeyboard()
    }

    fun hideKeyboard() {
        parentActivity?.hideKeyboard()
    }

    //region Handle Fragment
    companion object {
        private const val FRAGMENT_LEVEL_1 = "Level1"
        const val TRANSITION_DELAY = 300L

        var isPopBackStack = false
        var isSetFragmentInProcess = false
        var disableFragmentAnimation = false

        /**
         * Add fragment with higher level
         * @param fromBottomAnimation default is FALSE: fragment will appear from right to left; TRUE: fragment will appear from bottom to top
         */
        fun addFragment(activity: FragmentActivity?, fragment: BaseFragment, fromBottomAnimation: Boolean = false) {
            if (activity == null) {
                return
            }
            //Get current fragment level
            val currentFragment = activity.supportFragmentManager.findFragmentById(R.id.flContainer)
            var currentLevel = 1
            if (currentFragment != null) {
                currentFragment as BaseFragment
                currentLevel = currentFragment.getFragmentLevel() + 1
            }
            fragment.setFragmentLevel(currentLevel)

            if (fromBottomAnimation) {
                setFragment(
                        activity.supportFragmentManager,
                        fragment,
                        R.id.flContainer,
                        R.anim.slide_up,
                        R.anim.fade_out,
                        R.anim.fade_in,
                        R.anim.slide_down
                )
            } else {
                setFragment(activity.supportFragmentManager, fragment)
            }
        }

        fun setFragment(
                fragmentManager: FragmentManager, fragment: BaseFragment, containerViewId: Int = R.id.flContainer,
                animInEnter: Int = R.anim.anim_slide_in_left, animInExit: Int = R.anim.anim_slide_out_left,
                animOutEnter: Int = R.anim.anim_slide_in_right, animOutExit: Int = R.anim.anim_slide_out_right
        ) {
            if (isSetFragmentInProcess) {
                return
            }
            isSetFragmentInProcess = true
            Handler().postDelayed({
                popBackStack(fragmentManager, fragment)

                val fragmentTransaction = fragmentManager.beginTransaction()
                if (FRAGMENT_LEVEL_1 != fragment.getTransactionName()) {
                    fragmentTransaction.setCustomAnimations(animInEnter, animInExit, animOutEnter, animOutExit)
                } else {
                    fragmentTransaction.setCustomAnimations(0, animInExit, 0, 0)
                }
                fragmentTransaction.replace(containerViewId, fragment)
                try {
                    fragmentTransaction.addToBackStack(fragment.getTransactionName()).commit()
                } catch (e: Exception) {
                }
                isPopBackStack = false
                isSetFragmentInProcess = false
            }, TRANSITION_DELAY)
        }

        //Avoid reloading parent when try to replace same level fragment
        private fun handlePopBackStack(fragmentManager: FragmentManager, destinationFragment: BaseFragment) {
            try {
                val currentLevel =
                        (fragmentManager.findFragmentById(R.id.flContainer) as BaseFragment).getFragmentLevel()
                if (destinationFragment.getFragmentLevel() <= currentLevel) {
                    isPopBackStack = true
                }
            } catch (e: Exception) {
            }
        }

        @SuppressLint("CommitTransaction")
        fun popBackStack(
                fragmentManager: FragmentManager,
                fragment: BaseFragment = HomeFragment(),
                disableAnimation: Boolean = false
        ) {
            handlePopBackStack(fragmentManager, fragment)
            if (fragmentManager.backStackEntryCount == 0) {
                return
            }
            try {
                if (disableAnimation) {
                    disableFragmentAnimation = true
                    fragmentManager.popBackStackImmediate(
                            fragment.getTransactionName(),
                            FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )
                    disableFragmentAnimation = false
                } else {
                    fragmentManager.popBackStackImmediate(
                            fragment.getTransactionName(),
                            FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )
                    if (FRAGMENT_LEVEL_1 == fragment.getTransactionName()) {
                        fragmentManager.beginTransaction().setCustomAnimations(R.anim.anim_slide_in_left, 0, 0, 0)
                    }
                }
            } catch (e: Exception) {
            }
        }
    }
    //endregion
}
