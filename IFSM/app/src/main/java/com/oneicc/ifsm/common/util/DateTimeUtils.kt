package com.oneicc.ifsm.common.util

import android.text.TextUtils
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {
    var DEFAULT_LOCALE: Locale = Locale.US
    private val NULL_DATE_DEFAULT = Date(0)
    private val SQL_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", DEFAULT_LOCALE)
    private val SO_POST_FORMAT = SimpleDateFormat("yyyy-MM-dd", DEFAULT_LOCALE)

    fun convertFromSQLFormatToDate(dateString: String): Date? {
        if (TextUtils.isEmpty(dateString)) {
            return NULL_DATE_DEFAULT
        }
        try {
            return SQL_DATE_FORMAT.parse(dateString)
        } catch (e: Exception) {
            Log.e("DateTimeUtils", e.toString())
        }

        return NULL_DATE_DEFAULT
    }

    fun convertToSQLFormatString(date: Date): String {
        return SQL_DATE_FORMAT.format(date)
    }

    fun convertToSOPostFormatString(date: Date): String {
        return SO_POST_FORMAT.format(date)
    }

    fun getToday(): Date {
        return Date(Calendar.getInstance().timeInMillis)
    }
}