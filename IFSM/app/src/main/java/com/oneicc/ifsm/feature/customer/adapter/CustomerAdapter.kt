package com.oneicc.ifsm.feature.customer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseAdapter
import com.oneicc.ifsm.common.base.BaseViewHolder
import com.oneicc.ifsm.common.model.Customer
import kotlinx.android.synthetic.main.item_customer.view.*

class CustomerAdapter(data: ArrayList<Customer>) : BaseAdapter<Customer, CustomerAdapter.ViewHolder>(data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_customer, parent, false))
    }

    class ViewHolder(view: View) : BaseViewHolder<Customer>(view) {
        override fun bind(item: Customer, position: Int) {
            itemView.tvCardCode.text = item.customerCode
            itemView.tvCardName.text = item.customerName
        }
    }
}