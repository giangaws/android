package com.oneicc.ifsm.feature.home.model

import com.oneicc.ifsm.common.util.Constants
import com.oneicc.ifsm.common.view.model.ExpandChild

data class HomeExpandChild(
    val childTitle: String,
    val description: String,
    val icon: Int = Constants.DEFAULT_ICON,
    val onClick: () -> Unit = {}
) : ExpandChild(childTitle, Constants.CHILD_TYPE_HOME)