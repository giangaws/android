package com.oneicc.ifsm.common.view.model

import com.oneicc.ifsm.common.base.BaseFragment

data class FragmentPager(
    val fragment: BaseFragment,
    val title: String = ""
)