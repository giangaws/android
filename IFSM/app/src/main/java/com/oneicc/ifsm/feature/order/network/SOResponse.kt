package com.oneicc.ifsm.feature.order.network

import com.google.gson.annotations.SerializedName

data class SOResponse(
    @SerializedName("ErrorCode")
    val errorCode: Int?,
    @SerializedName("ErrorMessage")
    var errorMessage: String?,
    @SerializedName("RefCode")
    val refCode: String?
)