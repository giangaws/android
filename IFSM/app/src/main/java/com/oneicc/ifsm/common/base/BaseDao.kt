package com.oneicc.ifsm.common.base

import androidx.room.*

@Dao
interface BaseDao<T : BaseModel> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(model: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list: List<T>)

    @Update
    fun update(model: T)

    @Delete
    fun delete(model: T)
}