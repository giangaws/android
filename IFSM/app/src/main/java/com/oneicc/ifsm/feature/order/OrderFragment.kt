package com.oneicc.ifsm.feature.order

import android.app.AlertDialog
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.database.AppDatabase
import com.oneicc.ifsm.common.database.dao.ItemDao
import com.oneicc.ifsm.common.database.dao.StockDao
import com.oneicc.ifsm.common.model.Item
import com.oneicc.ifsm.common.model.SOLine
import com.oneicc.ifsm.common.model.Stock
import com.oneicc.ifsm.common.network.Api
import com.oneicc.ifsm.common.network.NetworkClient
import com.oneicc.ifsm.feature.order.adapter.ItemAdapter
import com.oneicc.ifsm.feature.order.model.ProductUiModel
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_order.*
import timber.log.Timber

class OrderFragment : BaseFragment(R.layout.fragment_order) {
    private var data = ArrayList<ProductUiModel>()
    private lateinit var adapter: ItemAdapter
    private lateinit var stockDao: StockDao
    private lateinit var itemDao: ItemDao

    override fun initData() {
        parentActivity?.setUpTitleAndBackToolbar(getString(R.string.item_title))

        stockDao = AppDatabase.getAppDataBase(context!!).stockDao()
        itemDao = AppDatabase.getAppDataBase(context!!).itemDao()

        adapter = ItemAdapter(data)
        syncStockData()
    }

    override fun initView() {
        btnSaveOrder.setOnClickListener {
            val lines = ArrayList<SOLine>()
            val cart = data.filter { it.quantity > 0 }
            cart.forEach {
                lines.add(SOLine(it))
            }

            addFragment(OrderConfirmFragment.getInstance(lines))
        }

        rvItem.addItemDecoration(DividerItemDecoration(rvItem.context, DividerItemDecoration.VERTICAL))
        rvItem.adapter = adapter
    }

    private fun loadItemData() {
        val itemDao = AppDatabase.getAppDataBase(context!!).itemDao()
        compositeDisposable.add(
            itemDao.getAllProductUiModelByWarehouseCode("10")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    data.clear()
                    data.addAll(it)
                    adapter.notifyDataSetChanged()
                }, {
                    Timber.e("getCustomerData -> $it")
                })
        )
    }

    private fun syncStockData() {
        val builder = AlertDialog.Builder(context)
        builder.setMessage("Đang lấy dữ liệu tồn kho, xin hãy đợi!")
        val dialog = builder.create()
        dialog.setCancelable(false)
        dialog.show()

        compositeDisposable.add(
            NetworkClient.getRetrofit(context!!).create(Api::class.java).getAllStock()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showData(it)
                    dialog.dismiss()
                    saveNewStockData(it)
                }, {
                    dialog.dismiss()
                    Toast.makeText(context, it.message.toString(), Toast.LENGTH_SHORT).show()
                    Timber.e("syncStockData -> $it")
                })
        )
    }

    private fun showData(stockList: List<Stock>) {
        data.clear()
        val miniMartItem = stockList.filter { it.whsCode == "10" }
        miniMartItem.forEach {
            data.add(ProductUiModel(Item(it.itemCode, it.itemName, false), ArrayList(listOf(it))))
        }
        adapter.notifyDataSetChanged()
    }

    private fun saveNewStockData(data: List<Stock>) {
        compositeDisposable.add(Completable.fromAction { stockDao.insertAll(data) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({
                Timber.i("Save stock data success")
            }, {
                Toast.makeText(context, it.message.toString(), Toast.LENGTH_SHORT).show()
                Timber.e("saveNewStockData -> $it")
            }))
    }
}