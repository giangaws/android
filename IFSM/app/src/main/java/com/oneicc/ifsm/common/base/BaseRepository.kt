package com.oneicc.ifsm.common.base

import com.oneicc.ifsm.common.network.ZipResult
import kotlinx.coroutines.*
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

open class BaseRepository {
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.IO
    val scope = CoroutineScope(coroutineContext)

    fun cancelAllRequests() = coroutineContext.cancel()

    suspend fun <T : Any> apiCall(
        call: suspend () -> T,
        onSuccess: (T) -> Unit,
        onError: (Throwable) -> Unit,
        onFinish: () -> Unit
    ) {
        try {
            val response = call.invoke()
            withContext(Dispatchers.Main) {
                Timber.d("onSuccess")
                onSuccess(response)
            }
        } catch (t: Throwable) {
            withContext(Dispatchers.Main) {
                Timber.d(t.toString())
                onError(t)
            }
        } finally {
            withContext(Dispatchers.Main) {
                Timber.d("onFinish")
                onFinish()
            }
        }
    }

    suspend fun <T1 : Any, T2 : Any> apiCallZip(
        call1: suspend () -> T1,
        call2: suspend () -> T2,
        onSuccess: (ZipResult<T1, T2>) -> Unit,
        onError: (Throwable) -> Unit,
        onFinish: () -> Unit
    ) {
        try {
            val response1 = call1.invoke()
            val response2 = call2.invoke()
            withContext(Dispatchers.Main) {
                Timber.d("onSuccess")
                onSuccess(ZipResult(response1, response2))
            }
        } catch (t: Throwable) {
            withContext(Dispatchers.Main) {
                Timber.d(t.toString())
                onError(t)
            }
        } finally {
            withContext(Dispatchers.Main) {
                Timber.d("onFinish")
                onFinish()
            }
        }
    }
}