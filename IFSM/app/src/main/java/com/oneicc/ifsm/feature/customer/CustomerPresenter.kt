package com.oneicc.ifsm.feature.customer

import com.oneicc.ifsm.common.database.AppDatabase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CustomerPresenter(val view: CustomerContract.View, val database: AppDatabase) : CustomerContract.Presenter {

    private val disposable = CompositeDisposable()

    override fun loadData() {
        view.showLoading()
        val customerDao = database.customerDao()
        disposable.add(
            customerDao.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    view.onLoadDataSuccess(it)
                    view.hideLoading()
                }, {
                    view.hideLoading()
                    view.onLoadDateFailure(it)
                })
        )
    }
}