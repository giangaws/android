package com.oneicc.ifsm.feature.home.viewholder

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.oneicc.ifsm.R
import com.oneicc.ifsm.app.MainActivity
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.model.CustomerExpandChild
import com.oneicc.ifsm.common.util.BusAction
import com.oneicc.ifsm.common.util.Constants
import com.oneicc.ifsm.feature.visit.VisitFragment
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import kotlinx.android.synthetic.main.item_expand_child_customer.view.*
import org.greenrobot.eventbus.EventBus

class CustomerChildViewHolder(val view: View) : ChildViewHolder(view) {
    fun bind(data: CustomerExpandChild, fragment: BaseFragment) {
        val customer = data.customer
        itemView.vStatus.setBackgroundColor(ContextCompat.getColor(itemView.context, data.getStatusColor()))
        itemView.tvCustomerName.text = customer.customerName
        itemView.tvCustomerCode.text = customer.customerCode
        itemView.tvAddress.text = customer.address

        val spanAddress = SpannableString(itemView.context.resources.getString(R.string.tv_contact, customer.contact))
        spanAddress.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(itemView.context, R.color.textGray)),
            0, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        itemView.tvContact.text = spanAddress

        val spanPhone = SpannableString(itemView.context.resources.getString(R.string.tv_phone, customer.phone))
        spanPhone.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(itemView.context, R.color.textGray)),
            0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        itemView.tvPhone.text = spanPhone

        if (customer.pictureUrl.isNotEmpty()) {
            Glide.with(itemView)
                .load(customer.pictureUrl)
                .centerInside()
                .override(Constants.DEFAULT_HOME_ICON_SIZE, Constants.DEFAULT_HOME_ICON_SIZE)
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.default_avatar)
                .into(itemView.imgPicture)
        }

        itemView.vCustomerExpandChild.setOnClickListener {
            MainActivity.currentCustomer = customer
            EventBus.getDefault().post(BusAction.DoAction(Constants.ACTION_START_VISIT))
            fragment.addFragment(VisitFragment())
        }

        itemView.tvMap.setOnClickListener {
            Toast.makeText(itemView.context, "Chức năng đang xây dựng", Toast.LENGTH_LONG).show()
        }

        itemView.tvPhone.setOnClickListener {
            Toast.makeText(itemView.context, "Chức năng đang xây dựng", Toast.LENGTH_LONG).show()
        }
    }
}