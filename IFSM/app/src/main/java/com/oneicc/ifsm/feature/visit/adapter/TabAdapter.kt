package com.oneicc.ifsm.feature.visit.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.oneicc.ifsm.common.view.model.FragmentPager

data class TabAdapter(val fragmentManager: FragmentManager, val list: List<FragmentPager>) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment = list[position].fragment

    override fun getPageTitle(position: Int): CharSequence? = list[position].title

    override fun getCount(): Int = list.size
}