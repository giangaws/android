package com.oneicc.ifsm.feature.home

import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseFragment
import com.oneicc.ifsm.common.model.Customer
import com.oneicc.ifsm.common.model.CustomerExpandChild
import com.oneicc.ifsm.common.view.model.ExpandChild
import com.oneicc.ifsm.common.view.model.ExpandHeader
import com.oneicc.ifsm.feature.customer.CustomerFragment
import com.oneicc.ifsm.feature.home.adapter.HomeExpandHeaderAdapter
import com.oneicc.ifsm.feature.home.model.HeaderOnlyExpandChild
import com.oneicc.ifsm.feature.home.model.HomeExpandChild
import com.oneicc.ifsm.feature.kpi.KpiFragment
import com.oneicc.ifsm.feature.stock.StockFragment
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment(R.layout.fragment_home) {
    private val customerInRouteVisitList = ArrayList<CustomerExpandChild>()
    private val customerOutRouteVisitList = ArrayList<CustomerExpandChild>()
    private lateinit var headerAdapter: HomeExpandHeaderAdapter

    companion object {
        const val DATA_KEY = "DATA_KEY_HOME"

        fun getInstance(customerList: Array<Customer>): HomeFragment {
            val fragment = HomeFragment()
            val arguments = Bundle()
            arguments.putParcelableArray(DATA_KEY, customerList)
            fragment.arguments = arguments
            return fragment
        }
    }

    override fun initData() {
        parentActivity?.setUpHomeToolbar()

        val customerList = arguments?.getParcelableArray(DATA_KEY)!!
        customerList.forEach { customer ->
            val expandChild = CustomerExpandChild(customer as Customer)
            if (expandChild.customer.outRoute) {
                customerOutRouteVisitList.add(expandChild)
            } else {
                customerInRouteVisitList.add(expandChild)
            }
        }

        headerAdapter = HomeExpandHeaderAdapter(this, setUpMainMenu())
    }

    override fun initView() {
        rvHome.addItemDecoration(DividerItemDecoration(rvHome.context, DividerItemDecoration.VERTICAL))
        rvHome.adapter = headerAdapter
    }

    private fun setUpMainMenu() = ArrayList(listOf(
            ExpandHeader(
                    "Check-in đầu ngày",
                    subListCheckIn(),
                    "Thực hiện các bước chuẩn bị bán hàng",
                    R.drawable.ic_check_in
            ),
            ExpandHeader(
                    "Lịch ghé thăm",
                    subListCustomer(),
                    description = "Thực hiện các hoạt động bán hàng",
                    icon = R.drawable.ic_route
            ),
            ExpandHeader(
                    "Báo cáo",
                    subListReport(),
                    "Báo cáo về KPI, doanh số bán hàng...",
                    R.drawable.ic_form
            ),
            ExpandHeader(
                    "Quản lý khách hàng",
                    description = "Danh sách khách hàng đang quản lý",
                    icon = R.drawable.ic_address_book,
                    onClick = { addFragment(CustomerFragment()) }
            ),
            ExpandHeader(
                    "Quản lý kho",
                    description = "Danh sách kho và sản phẩm",
                    icon = R.drawable.ic_box,
                    onClick = { addFragment(StockFragment()) }
            ),
            ExpandHeader(
                    "Tư liệu, hình ảnh, video clips",
                    description = "Thư viện media phục vụ bán hàng",
                    icon = R.drawable.ic_image
            ),
            ExpandHeader(
                    "Check-out",
                    description = "Kết thúc hoạt động bán hàng hằng ngày",
                    icon = R.drawable.ic_lock
            )
    ))

    private fun subListCheckIn() = ArrayList<ExpandChild>(
            listOf(
                    HomeExpandChild(
                            "Chỉ số",
                            "Chỉ số làm việc",
                            onClick = { addFragment(KpiFragment()) }
                    ),
                    HomeExpandChild(
                            "Đơn hàng",
                            "Tình trạng đơn hàng"
                    ),
                    HomeExpandChild(
                            "Vấn đề",
                            "Danh sách vấn đề"
                    ),
                    HomeExpandChild(
                            "Khuyến mãi",
                            "Chương trình Khuyến mãi / Tích lũy"
                    ),
                    HomeExpandChild(
                            "Trả thưởng",
                            "Trả thưởng cho khách hàng"
                    ),
                    HomeExpandChild(
                            "Công việc",
                            "Công việc cần thực hiện"
                    ),
                    HomeExpandChild(
                            "Phân bổ",
                            "Phân bổ bán hàng, khuyến mãi"
                    )
            )
    )

    private fun subListCustomer(): ArrayList<ExpandChild> {
        val visitList = ArrayList<ExpandChild>()
        visitList.add(HeaderOnlyExpandChild(resources.getString(R.string.dung_tuyen)))
        visitList.addAll(customerInRouteVisitList)
        visitList.add(HeaderOnlyExpandChild(resources.getString(R.string.trai_tuyen)))
        visitList.addAll(customerOutRouteVisitList)
        return visitList
    }

    private fun subListReport() = ArrayList<ExpandChild>(
            listOf(
                    HomeExpandChild(
                            "KPI",
                            "Chỉ tiêu đánh giá hiệu quả công việc"
                    ),
                    HomeExpandChild(
                            "Đơn hàng",
                            "Tổng hợp đơn hàng"
                    ),
                    HomeExpandChild(
                            "Doanh số",
                            "Tổng hợp doanh số bán hàng"
                    ),
                    HomeExpandChild(
                            "Vấn đề",
                            "Phản hồi từ khách hàng"
                    )
            )
    )
}