package com.oneicc.ifsm.common.network

import android.content.Context
import com.oneicc.ifsm.BuildConfig
import com.oneicc.ifsm.app.App
import com.oneicc.ifsm.common.util.Constants
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetworkClient {
    private fun getClient(needToken: Boolean, context: Context): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        if (needToken) {
            httpClient.addInterceptor { chain ->
                val request = chain.request().newBuilder().addHeader("AccessToken", Constants.ACCESS_TOKEN).build()
                chain.proceed(request)
            }
        }
        httpClient.connectTimeout(Constants.CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging)
        }
        httpClient.addInterceptor(ChuckInterceptor(context))
        return httpClient.build()
    }

    fun getRetrofit(context: Context): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://103.70.28.116:8088")
            .client(getClient(false, context))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getRetrofitForTracking(context: Context): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://103.70.28.116:10001")
            .client(getClient(true, context))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}