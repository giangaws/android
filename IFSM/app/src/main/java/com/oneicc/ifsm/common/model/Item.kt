package com.oneicc.ifsm.common.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.oneicc.ifsm.common.base.BaseModel

@Entity(indices = [Index("itemCode")])
data class Item(
    @PrimaryKey
    val itemCode: String = "",
    val itemName: String = "",
    val isPromotion: Boolean = false
) : BaseModel()