package com.oneicc.ifsm.common.base

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class BaseModel : Parcelable