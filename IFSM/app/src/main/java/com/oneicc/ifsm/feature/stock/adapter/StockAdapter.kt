package com.oneicc.ifsm.feature.stock.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.base.BaseAdapter
import com.oneicc.ifsm.common.base.BaseViewHolder
import com.oneicc.ifsm.common.model.Stock
import kotlinx.android.synthetic.main.item_stock.view.*

class StockAdapter(data: ArrayList<Stock>) : BaseAdapter<Stock, StockAdapter.ViewHolder>(data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_stock, parent, false))
    }

    class ViewHolder(view: View) : BaseViewHolder<Stock>(view) {
        override fun bind(item: Stock, position: Int) {
            itemView.tvItemCode.text = item.itemCode
            itemView.tvItemName.text = item.itemName
            itemView.tvQuantity.text = item.inStock.toString()
            itemView.tvWarehouseCode.text = item.whsName
        }
    }
}