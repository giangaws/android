package com.oneicc.ifsm.feature.order.model

import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation
import com.oneicc.ifsm.common.base.BaseModel
import com.oneicc.ifsm.common.model.Item
import com.oneicc.ifsm.common.model.Stock

data class ProductUiModel(
    @Embedded
    var item: Item = Item(),
    @Relation(parentColumn = "itemCode", entityColumn = "itemCode")
    var stock: List<Stock> = ArrayList(),
    @Ignore
    var quantity: Int = 0
) : BaseModel()