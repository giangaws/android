package com.oneicc.ifsm.common.model

import com.oneicc.ifsm.R
import com.oneicc.ifsm.common.util.Constants
import com.oneicc.ifsm.common.view.model.ExpandChild

data class CustomerExpandChild(
    val customer: Customer
) : ExpandChild(customer.customerName, Constants.CHILD_TYPE_CUSTOMER) {
    fun getStatusColor(): Int {
        return when (customer.visitStatus) {
            Constants.VISIT_STATUS_HAS_ORDER -> R.color.colorVisitStatusHasOrder
            Constants.VISIT_STATUS_NO_ORDER -> R.color.colorVisitStatusNoOrder
            Constants.VISIT_STATUS_CANNOT_VISIT -> R.color.colorVisitStatusCannotVisit
            else -> R.color.colorVisitStatusNotYet
        }
    }
}