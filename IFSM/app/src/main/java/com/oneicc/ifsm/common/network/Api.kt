package com.oneicc.ifsm.common.network

import com.oneicc.ifsm.common.model.Stock
import com.oneicc.ifsm.feature.order.network.SOResponse
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface Api {
    @POST("/api/SOs")
    fun postOrder(@Body requestBody: RequestBody): Single<SOResponse>

    @GET("/api/WareHouses")
    fun getAllStock(): Single<List<Stock>>

    @POST("/api/EmployeeActivity/add")
    fun addActivity(@Body requestBody: RequestBody): Single<ApiResponse<Int>>

    @POST("/api/EmployeeActivity/update")
    fun updateActivity(@Body requestBody: RequestBody): Single<ApiResponse<Int>>
}