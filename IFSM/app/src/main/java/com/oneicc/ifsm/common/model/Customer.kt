package com.oneicc.ifsm.common.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.oneicc.ifsm.common.base.BaseModel
import com.oneicc.ifsm.common.util.Constants

@Entity(indices = [Index("customerCode")])
data class Customer(
    val customerName: String = "",
    @PrimaryKey
    val customerCode: String = "",
    val address: String = "",
    val contact: String = "",
    val phone: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val pictureUrl: String = "",
    val lastMonthRevenue: Double = 55660000.0,
    val revenue: Double = 10300000.0,
    val debt: Double = 4300000.0,
    var visitStatus: Int = Constants.VISIT_STATUS_NOT_YET,
    val outRoute: Boolean = false
) : BaseModel()