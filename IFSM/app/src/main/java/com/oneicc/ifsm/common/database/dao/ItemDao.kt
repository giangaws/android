package com.oneicc.ifsm.common.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.oneicc.ifsm.common.base.BaseDao
import com.oneicc.ifsm.common.model.Item
import com.oneicc.ifsm.feature.order.model.ProductUiModel
import io.reactivex.Single

@Dao
interface ItemDao : BaseDao<Item> {
    @Query("SELECT * From Item")
    fun getAll(): Single<List<Item>>

    @Query("SELECT * From Item")
    fun getAllProductUiModel(): Single<List<ProductUiModel>>

    @Query("SELECT * From Item i, Stock s WHERE whsCode = :whsCode AND i.itemCode = s.itemCode ORDER BY s.whsCode")
    fun getAllProductUiModelByWarehouseCode(whsCode: String): Single<List<ProductUiModel>>
}