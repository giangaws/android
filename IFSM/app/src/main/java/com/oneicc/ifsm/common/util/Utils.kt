package com.oneicc.ifsm.common.util

import android.content.Context
import android.content.res.Resources
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import timber.log.Timber
import java.io.File

object Utils {
    fun dpToPx(dp: Int): Int = (dp * Resources.getSystem().displayMetrics.density).toInt()

    fun pxToDp(px: Int): Int = (px / Resources.getSystem().displayMetrics.density).toInt()

    fun gson(): Gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()

    fun exportDatabase(context: Context) {
        try {
            val targetPath = context.getExternalFilesDir("")
            if (targetPath!!.canWrite()) {
                val currentPath = context.getDatabasePath(Constants.DATABASE_NAME).absolutePath
                Timber.i("exportDatabase() currentPath: $currentPath")
                val currentFile = context.getDatabasePath(Constants.DATABASE_NAME)

                val targetFileName = Constants.DATABASE_NAME + ".db"
                val targetFile = File(targetPath, targetFileName)
                Timber.i("exportDatabase() targetPath: ${targetFile.absolutePath}")

                if (currentFile.exists()) {
                    currentFile.copyTo(targetFile, true)
                }
            }
        } catch (e: Exception) {
            Timber.e("exportDatabase() $e")
        }
    }
}