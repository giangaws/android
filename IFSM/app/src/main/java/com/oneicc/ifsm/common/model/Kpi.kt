package com.oneicc.ifsm.common.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.oneicc.ifsm.common.base.BaseModel

@Entity(indices = [Index("kpiCode")])
data class Kpi(
        @PrimaryKey
        val kpiCode: String = "",
        val kpiName: String = "",
        val target: Double = 0.0,
        val current: Double = 0.0,
        val complete: Boolean = false,
        val startDate: String = "1980-01-01",
        val endDate: String = "1980-01-01",
        val todayTarget: Double = 0.0
) : BaseModel()