package com.oneicc.ifsm.common.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.oneicc.ifsm.common.base.BaseModel

@Entity(indices = [Index("cardCode")])
data class Revenue(
        @PrimaryKey
        val cardCode: String = "",
        val cardName: String = "",
        val totalAmount: Double = 0.0,
        val lastOrderDate: String = "1980-01-01"
) : BaseModel()