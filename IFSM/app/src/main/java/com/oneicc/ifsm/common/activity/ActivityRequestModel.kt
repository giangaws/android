package com.oneicc.ifsm.common.activity

import com.google.gson.annotations.Expose

data class ActivityRequestModel(
    @Expose
    val input: ActivityModel
)