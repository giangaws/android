package com.oneicc.ifsm.common.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.oneicc.ifsm.common.base.BaseModel
import com.oneicc.ifsm.feature.order.model.ProductUiModel

@Entity(
    indices = [Index("itemCode", "headerId")],
    foreignKeys = [
        ForeignKey(entity = Item::class, parentColumns = ["itemCode"], childColumns = ["itemCode"]),
        ForeignKey(entity = SOHeader::class, parentColumns = ["id"], childColumns = ["headerId"])
    ]
)
data class SOLine(
    @PrimaryKey(autoGenerate = true)
    var id: Int?,
    var headerId: Int?,
    @Expose
    val itemCode: String, //SKU
    @Expose
    val itemName: String, //Tên SKU
    @Expose
    var quantity: Int = 0, //Số lượng
    @Expose
    val whsCode: String, //Kho
    @Expose
    val lineTotal: Double //Giá trị line sau thuế
) : BaseModel() {
    constructor(model: ProductUiModel) : this(
        null,
        null,
        model.item.itemCode,
        model.item.itemName,
        model.quantity,
        model.stock[0].whsCode,
        model.quantity * model.stock[0].price
    )
}