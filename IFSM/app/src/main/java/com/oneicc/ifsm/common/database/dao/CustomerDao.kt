package com.oneicc.ifsm.common.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.oneicc.ifsm.common.base.BaseDao
import com.oneicc.ifsm.common.model.Customer
import io.reactivex.Single

@Dao
interface CustomerDao : BaseDao<Customer> {
    @Query("SELECT * FROM Customer WHERE CustomerCode = :customerCode")
    fun getById(customerCode: String): Single<Customer>

    @Query("SELECT * FROM Customer ORDER BY CustomerCode ASC LIMIT 1")
    fun getFirst(): Single<Customer>

    @Query("SELECT * From Customer")
    fun getAll(): Single<List<Customer>>
}