package com.oneicc.ifsm.common.base

interface BaseApiView : BaseView {
    fun onCallApiSuccess()
    fun onCallApiFailure()
    fun onCallApiFinished()
}